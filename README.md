#How to useB

1. remove or rename ~/.emacs.d and ~/.emacs

2. clone it as .emacs.d
  * clone https://github.com/veto64/emacs .emacs.d

3. create syslink
  * cd .emacs.d
  * ./link_emacs.sh
  * or copy the .emacs file to your home directory 