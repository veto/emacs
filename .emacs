(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa" . "http://melpa.org/packages/")
   t)
  (package-initialize))

(progn
  (require 'windmove)
  ;; use Shift+arrow_keys to move cursor around split panes
  (windmove-default-keybindings)
  ;; when cursor is on edge, move to the other side, as in a torus space
  (setq windmove-wrap-around t )
  )

;;; Better buffer switching
(iswitchb-mode 1)
(add-to-list 'load-path "~/.emacs.d/lisp/")
;(require 'ps-ccrypt)
(setq auto-mode-alist  (cons '(".rhtml$" . html-mode) auto-mode-alist))

(setq auto-mode-alist
  (append '(("\\.php$" . php-mode)
            ("\\.ctp$" . php-mode)
            ("\\.tpl$" . php-mode))
              auto-mode-alist))
(setq-default c-electric-flag nil)

(windmove-default-keybindings)         ; shifted arrow keys


(put 'scroll-left 'disabled nil)


(add-to-list 'load-path "~/.emacs.d/emacs-nav-49/")
(require 'nav)
(nav-disable-overeager-window-splitting)
;; Optional: set up a quick key to toggle nav
;; (global-set-key [f8] 'nav-toggle)

(global-set-key (kbd "C-x C-x") 'my-list-buffers)
(defun my-list-buffers (&optional files-only)
  "Display a list of names of existing buffers.
The list is displayed in a buffer named `*Buffer List*'.
Note that buffers with names starting with spaces are omitted.
Non-null optional arg FILES-ONLY means mention only file buffers.

For more information, see the function `buffer-menu'."
  (interactive "P")
  (switch-to-buffer (list-buffers-noselect files-only)))


(require 'bs)
(global-set-key "\C-x\C-b" 'bs-show)


(autoload 'scss-mode "scss-mode")
(add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode))
(setq-default scss-compile-at-save nil)

(autoload 'xahk-mode "xahk-mode")
(add-to-list 'auto-mode-alist '("\\.ahk\\'" . xahk-mode))


(setq php-mode-warn-if-mumamo-off nil)
(electric-indent-mode 0)


(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))


(add-hook 'python-mode-hook 'anaconda-eldoc-mode)
(add-hook 'python-mode-hook 'anaconda-mode)
(add-hook 'after-init-hook 'global-company-mode)
(eval-after-load 'company
(progn
    '(add-to-list 'company-backends 'company-anaconda)
    ))



(require 'company)
(require 'company-tern)

(add-to-list 'company-backends 'company-tern)
(add-hook 'js2-mode-hook (lambda ()
                           (tern-mode)
                           (company-mode)))
                           
;; Disable completion keybindings, as we use xref-js2 instead
(define-key tern-mode-keymap (kbd "M-.") nil)





;;(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))













(package-initialize)

(cond
    ((string-equal system-type "windows-nt") 
        (progn
            ; Windows-specific configurations

        )
    )
    ((string-equal system-type "gnu/linux")
        (progn
            ; Linux-specific configurations

        )
    )
    ((string-equal system-type "darwin")
        (progn

	  
(add-hook 'shell-mode-hook
  (lambda ()
    (define-key shell-mode-map (kbd "<M-up>") 'comint-previous-input)
    (define-key shell-mode-map (kbd "<M-down>") 'comint-next-input)
  )
)

(add-to-list 'exec-path "/usr/local/bin")


        )
    )
)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (php-mode js2-mode company-tern company-anaconda))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
