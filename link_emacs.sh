#!/bin/bash
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
parentdir="$(dirname "$dir")"
source="${dir}/.emacs"
target="${parentdir}/.emacs"

echo "...remove link"
rm "${target}"

echo "...created link"
ln -s "${source}" "${target}"

echo "...test created syslink .emacs:"
if [ ! \( -e "${target}" \) ]
then
     echo "%ERROR: file ${target} does not exist!" >&2
     exit 1
elif [ ! \( -f "${target}" \) ]
then
     echo "%ERROR: ${target} is not a file!" >&2
     exit 2
elif [ ! \( -r "${target}" \) ]
then
     echo "%ERROR: file ${target} is not readable!" >&2
     exit 3
elif [ ! \( -s "${target}" \) ]
then
     echo "%ERROR: file ${target} is empty!" >&2
     exit 4
else
  echo "success link ${target} exists !"
fi

